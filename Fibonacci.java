import java.util.Scanner;

public class Fibonacci{
    static int n0=1,n1=1,n2=0;
    static void printFibonacci(int count){
        if(count>0) {
            n2 = n0 + n1;
            n0 = n1;
            n1 = n2;

            printFibonacci(count - 1);
            if (count == 1) {
                System.out.print(n1);
            }
        }
    }
    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);

        int count=sc.nextInt();
        printFibonacci(count-1);
        if(count==1 || count==0){
            System.out.println(1);
        }
    }
}  